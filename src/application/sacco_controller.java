package application;

import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;

public class sacco_controller {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private JFXTextField principal;

    @FXML
    private JFXTextField time;

    @FXML
    private JFXTextField rate;

    @FXML
    private JFXTextField interest;
    
    @FXML
    private JFXTextField amount_payable;

    @FXML
    void calculate(ActionEvent event) {
    	
    	int p = Integer.parseInt(principal.getText());
    	int rte = Integer.parseInt(rate.getText());
    	int tme  = Integer.parseInt(time.getText());
    	
    	float i = (p*rte*tme)/100;
    	
    	float sum = i + p;
    	
    	interest.setText(String.valueOf(i));
    	amount_payable.setText(String.valueOf(sum));

    }
    
    @FXML
    void close(ActionEvent event) {
    	System.out.println("clicked close");
    	System.exit(0);
    }

    @FXML
    void exit(ActionEvent event) {
    	System.out.println("clicked exit");
    	System.exit(0);
    }

    @FXML
    void initialize() {
        assert principal != null : "fx:id=\"principal\" was not injected: check your FXML file 'sacco.fxml'.";
        assert amount_payable != null : "fx:id=\"amount_payable\" was not injected: check your FXML file 'sacco.fxml'.";

    }
}
